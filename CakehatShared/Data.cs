﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.ComponentModel;

namespace Cakehat
{
    public enum LatePassPolicy
    {
        OnePassIfLate,
        PassPerDay,
        NoPass
    }

    public enum LatePolicy
    {
        DailyDeduction,
        EarlyOntimeLate
    }

    public class AsgnInfo
    {
        public AsgnInfo()
        {
            AsgnName = "Assessment";
            LatePolicyType = LatePolicy.DailyDeduction;
            LatePolicyParameter = 10;
            EarlyBonusDate = DateTime.MinValue;
            DueDate = DateTime.Now;
            LateDate = DateTime.MaxValue;
            LatePassEnabled = true;
            GradeSheetCategories = new List<GradeSheetCategory> { };
        }

        [XmlAttribute()]
        public string AsgnName { get; set; }

        [XmlElement()]
        public LatePolicy LatePolicyType { get; set; }

        [XmlElement()]
        public double LatePolicyParameter { get; set; }

        [XmlElement()]
        public DateTime EarlyBonusDate { get; set; }

        [XmlElement()]
        public DateTime DueDate { get; set; }

        [XmlElement()]
        public DateTime LateDate { get; set; }

        [XmlElement()]
        public bool LatePassEnabled { get; set; }

        [XmlArray()]
        public List<GradeSheetCategory> GradeSheetCategories { get; set; }


        [XmlIgnore()]
        public double PointsPossible { get { return GradeSheetCategories.Sum(a => a.Items.Sum(b => b.PointsPossible)); } }

        [XmlIgnore()]
        public double PointsEarned { get { return GradeSheetCategories.Sum(a => a.Items.Sum(b => b.PointsEarned)); } }
    }


    public class GradeSheetCategory
    {
        public GradeSheetCategory()
        {
            Name = "";
            Items = new List<GradeSheetItem>();
        }

        [XmlAttribute()]
        public string Name { get; set; }

        [XmlArray()]
        public List<GradeSheetItem> Items { get; set; }
    }

    public class GradeSheetItem
    {
        [XmlAttribute()]
        public string Description { get; set; }

        [XmlAttribute()]
        public double PointsPossible { get; set; }

        [XmlAttribute()]
        public double PointsEarned { get; set; }

        [XmlElement()]
        public string GraderComment { get; set; }
    }

    public class TAInfo
    {
        [XmlAttribute("Login")]
        public string TALogin { get; set; }

        [XmlArray("Blacklisted")]
        public List<string> BlacklistStudents { get; set; }

        public static TAInfo GetCurrentTA()
        {
            string username = Environment.UserName;
            //TODO: Actually load the XML file for the current TA. 

            return new TAInfo()
            {
                TALogin = "bherila",
                BlacklistStudents = new List<string>() {
                    "aunger", 
                    "jnfreema",
                    "rlaplant"
                }
            };
        }
    }
}
