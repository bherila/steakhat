﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.IO;

namespace Cakehat
{

    /* here is the distributor program */
    public class Distributor
    {

        /// <summary>
        /// Usage: ./distributor [asgn]
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            string asgn;
            if (args.Length == 0 || string.IsNullOrEmpty(args[0]))
            {
                Console.WriteLine("usage: ./distributor <asgn>");
                return;
            }
            else
            {
                asgn = args[0];
                Console.WriteLine(String.Format(@"Creating distribution for assignment ""{0}""", asgn));
            }

            DirectoryInfo currentFolder = new FileInfo(System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName).Directory;
            string configPath = Path.Combine(currentFolder.Parent.FullName, "config");
            string distrPath = Path.Combine(currentFolder.Parent.FullName, "distributions");
            string taConfigPath = Path.Combine(configPath, "ta");
            string handinPath = Path.Combine("/course/cs032/handin", asgn);
            


            // check that directories exist
            if (!Directory.Exists(handinPath))
            {
                Console.WriteLine(String.Format("Warning: Path {0} does not exist, using current directory {1} for handin src.", handinPath, Environment.CurrentDirectory));
                handinPath = Environment.CurrentDirectory;
            }

            if (!Directory.Exists(taConfigPath))
            {
                Console.WriteLine(String.Format("Warning: Path {0} does not exist, using current directory {1} for ta-info src.", taConfigPath, Environment.CurrentDirectory));
                taConfigPath = Environment.CurrentDirectory;
            }

            if (!Directory.Exists(distrPath))
            {
                Console.WriteLine(String.Format("Warning: Path {0} does not exist; handin files will not be copied.", distrPath));
                distrPath = null;
            }
            else
            {
                distrPath = Path.Combine(distrPath, asgn);
                if (!Directory.Exists(distrPath))
                    Directory.CreateDirectory(distrPath);
            }



            List<TAInfo> TAList = new List<TAInfo>(); 
            XmlSerializer xml = new XmlSerializer(typeof(TAInfo));
            foreach (var taFile in Directory.GetFiles(taConfigPath, "*.xml")) //Get all TAs by loading XML from ../config/ta/*.xml
            {
                Console.WriteLine("Reading TA file {0}", Path.GetFileName(taFile));
                StreamReader textFile = File.OpenText(taFile);
                TAList.Add((TAInfo)xml.Deserialize(textFile)); // this is how we read the XML
                textFile.Close();
            }

            // get things in the handin folder for this assignment. 
            Random rnd = new Random();
            string[] handinFiles = Directory.GetFiles(handinPath, "*.tgz");
            string[] shuffledHandins = handinFiles.OrderBy(tmp => rnd.Next()).ToArray();

            var handinsPerTA = from ta in TAList
                               select new Tuple<string, IEnumerable<string>>
                               (
                                   /* first item in the tuple is the TA Login */ 
                                   ta.TALogin,

                                   /* second item in the tuple is the list of FULL HANDIN FILENAMES that the TA has */ 
                                   from handin in shuffledHandins
                                   let handin_login = Path.GetFileNameWithoutExtension(handin).ToLower()
                                   where !ta.BlacklistStudents.Contains(handin_login, StringComparer.OrdinalIgnoreCase)
                                   select handin
                               );

            // TODO: make the assignment algorithm better
            IEnumerable<Tuple<string, IEnumerable<string>>> distributionResult = handinsPerTA;


            // TODO: now dump out the answer by copying 1 tgz file per TA handin
            foreach (var taDistribution in distributionResult)
            {
                string taDistrPath = null;
                if (distrPath != null)
                {
                    taDistrPath = Path.Combine(distrPath, taDistribution.Item1 /* ta name */);
                    if (!Directory.Exists(taDistrPath))
                        Directory.CreateDirectory(taDistrPath);
                }

                Console.WriteLine("Assignments for {0}", taDistribution.Item1);
                int i = 0;
                foreach (string handinFilename in taDistribution.Item2 /* list of distributed students */)
                {
                    Console.WriteLine("  {0}. {1}", (++i), Path.GetFileNameWithoutExtension(handinFilename));
                    if (taDistrPath != null)
                    {
                        FileInfo OriginalHandin = new FileInfo(handinFilename);
                        FileInfo DistributedHandin = new FileInfo(Path.Combine(taDistrPath, Path.GetFileName(handinFilename)));

                        try
                        {
                            OriginalHandin.CopyTo(DistributedHandin.FullName, true /*overwrite*/);
                        }
                        catch (Exception x)
                        {
                            Console.WriteLine("Error copying file {0}: Exception -- {1} ", handinFilename, x.ToString());
                        }
                        try
                        {
                            DistributedHandin.CreationTimeUtc = OriginalHandin.CreationTimeUtc;
                            DistributedHandin.LastAccessTimeUtc = OriginalHandin.LastAccessTimeUtc;
                            DistributedHandin.LastWriteTimeUtc = OriginalHandin.LastWriteTimeUtc;
                        }
                        catch
                        {
                            Console.WriteLine("Warning: Failed to update timestamps");
                        }
                    }
                }
            }

            Console.ReadKey();
        }

    }


}
